; matrix86 is a real mode MBR demo inspired by cmatrix(1).

; Streams of random green glyphs run down the screen in spaced columns,
; where the glyphs themselves are always stationary. With white tips,
; both the streams and the gaps between them are of random lengths.

; This program requires an IBM Personal Computer/AT or compatible with
; an Intel 80286 or newer and an EGA, VGA, or compatible graphics card.
; INT 15h, 86h appears to hang when using a BIOS older than 1985-11-15.

bits 16
org 0x7C00

; Consider this fictitious example of a frame buffer below, where the O
; characters are green glyphs, and the V characters are white glyphs.
; The constants below would then be 12, 6, 72, 6, and 36 respectively.

; +------------+
; |O O   O O   |
; |O O O O V   |
; |O V O O   O |
; |O   O O   O |
; |V   V O O V |
; |      V V   |
; +------------+

W:   equ 80      ; Width of the frame buffer
H:   equ 25      ; Height of the frame buffer
C:   equ W * H   ; Number of characters in the frame buffer
N:   equ W / 2   ; Number of columns in the matrix
G:   equ N * H   ; Number of glyphs in the matrix

; The region of constants below is used to minimise the amount of code
; required to load magic words en masse, mostly in conjunction with the
; POPA instruction. The region can ultimately be reused as the stack,
; much like how MBR stacks often start at 0x7C00 and grow downwards.

; Not all of the region is reused for the stack however, because POPA
; wastes the word that the PUSHA instruction uses for the SP register.
; By starting the stack just above that word, we can use it to load an
; initial value for random_lfsr without any instructions.

entry_point:

	; Some computers jump to CS:IP = 0x0000:0x7C00 to complete the
	; boot process, while others instead jump to 0x07C0:0x0000.
	; Which segment:address pair is used doesn't actually matter
	; for this program, because the only jumps and calls used are
	; relative to IP. Therefore, a far jump is not needed.

	jmp short matrix_main

constants:

	; INT 0x10 AH 0x00: VGA text mode 0x03
	dw 0x0003      ; AX: function code | video mode

	; INT 0x10 AH 0x01: Invisible cursor
	dw 0x0100      ; AX: function code | unused
	dw 0x2607      ; CX: scan row start | scan row end

	; Allow for easy access to the VGA frame buffer
	dw 0xB800      ; ES: Base segment address

	; One half of the POPA register block
	dw C * 2 - 2   ; DI: Offset of the frame buffer's last word
	dw C * 2 - 2   ; SI: Offset of the frame buffer's last word
	dw after_state ; BP: Initial cursor into column_state
	dw 0x2000      ; SP: Initial value for random_lfsr

	stack_base_here:

	; The other half of the POPA register block
	dw 0x046C      ; BX: Address of the 8253 tick counter
	dw 0x6666      ; DX: INT 0x15 AH 0x86: Wait for DX microseconds
	dw G           ; CX: Number of glyphs in the matrix
	dw N           ; AX: Number of columns in the matrix

; For each column, we keep track of whether or not its upper character
; will be visible when drawing the next frame, and how many frames are
; remaining in that visibility state. Enter column_state, a vector that
; maps a column number [0..39] to an octet representing these concepts:

; [ 7 ] [ 6     5     4     3     2     1     0 ]
; [ v ] [     t = frames remaining in state     ]

; Before drawing each frame, t is decremented if it's positive.
; Otherwise t is reset to a random value, and v is toggled.

stack_base:     equ stack_base_here     ; 0x7C12
random_lfsr:    equ stack_base - 2      ; 0x7C10 to 0x7C12 = 0x02
column_state:   equ random_lfsr - N     ; 0x7BE8 to 0x7C10 = 0x27
after_state:    equ column_state + N    ; 0x7C10
stack_top:      equ column_state        ; 0x7BE8

; matrix_main()
; Configures VGA, the registers, the stack, and other state.

matrix_main:

	; Sane defaults required for some of matrix_main
	xor ax,ax
	mov ss,ax
	mov ds,ax

	; Abuse the stack to load constants
	mov sp,constants

	; INT 0x10 AH 0x00: VGA text mode 0x03
	; 4 pages of 80 x 25 characters from 0xB800
	; Each character is 9 x 16 with 16 colours available
	pop ax
	int 0x10

	; INT 0x10 AH 0x01: Invisible cursor
	pop ax
	pop cx
	int 0x10

	; Load eight more constants
	pop es
	popa

	; Configure the stack proper
	mov sp,stack_top

	; random_lfsr = (8253 tick counter) OR 0x2000
	; We forcibly enable a bit by default to avoid seeding the LFSR
	; with zero, which would be a pathological state.
	mov bx,[bx]
	or [random_lfsr],bx

	; Make DS also point to the VGA text buffer
	push es
	pop ds

	; Initialise column_state
	call matrix_update

	; Now that the meat of the matrix is about to begin, ensure
	; that all STOS and LODS instructions iterate backwards, so
	; that the frame buffer is in a consistent state when we are
	; looking upwards, deciding whether or not to show a glyph.
	std

	; Fall through to matrix_frame()

; matrix_frame()
; Updates column_state, redraws the frame buffer, then pauses to show
; off the resultant masterpiece. Rinse and repeat.

matrix_frame:

	pusha
		; Update column_state
		call matrix_update

		; For all of the pairs of characters
		.pair:
			; Copy the blank character without a fuss
			movsw

			; Read the matrix character
			lodsw

			; Have we reached the top row?
			cmp cx,N
			jbe .top
				; ZF = Is the glyph above us hidden?
				cmp byte [di - (W * 2 - 1)],0
				jmp short .top_out
			.top:
				; Move the column_state cursor
				dec bp

				; ZF = Is the column state v = 0?
				test byte [bp],0x80
			.top_out:

			; Are we showing or hiding the glyph?
			jz .hide
				; White
				mov dh,0x0F

				; Determine whether this is a tip by
				; checking if the glyph was visible.
				cmp ah,0

				; So the glyph was not a tip?
				jz .tip
					; Green
					mov dh,0x02
				.tip:

				; Colour that glyph!
				mov ah,dh
				jmp short .hide_out
			.hide:
				; AX = Random word
				call random

				; Clip values to [33,127). DIV CX means
				; that (AX, DX) = divmod(DX:AX, CX).
				xor dx,dx
				push cx
				mov cx,94
				div cx
				pop cx
				xchg ax,dx
				add al,33
			.hide_out:

			; Write the matrix character
			stosw
		loop .pair
	popa
	pusha
		; INT 0x15 AH 0x86: Wait for DX microseconds
		xor cx,cx
		mov ax,0x8600
		int 0x15
	popa

jmp short matrix_frame

; matrix_update()
; Updates or initialises column_state. If we are initialising the
; buffer, then set every octet such that v = 0 and t is a random value
; in [0, 32). Otherwise, decrement t in every octet that has not
; expired, and set every octet which has expired such that v is toggled
; and t is a new random value. We use the DF flag, which is set by STD,
; to distinguish initialisation and update scenarios.

matrix_update:

	pusha
		; Loop CX times
		xchg cx,ax
		.column:
			; Move the column_state cursor
			dec bp

			; Stack from top: [0, 32)
			call random
			and ax,0x1F
			push ax

			; Stack from top: (state) [0, 32)
			push word [bp]

			; Check the direction flag
			pushf
			pop ax
			test ax,0x0400

			; DL = Column state octet
			pop dx

			; AL = Random number in [0, 32)
			pop ax

			; Was DF set? Are we updating?
			jz .initialising
				; Check the value of t
				test dl,0x7F

				; Are we alive or expired?
				jz .expired
					; Tick t down again
					dec dx
					jmp short .expired_out
				.expired:
					; Toggle v and reset t
					or al,0x80
					xor dl,al
				.expired_out:
				jmp short .initialising_out
			.initialising:
				; Just accept the random number because
				; conveniently enough, v = 0
				xchg dx,ax
			.initialising_out:

			; Save the new column state octet
			mov byte [bp],dl
		loop .column
	popa

ret

; random(out AX)
; Using a Galois LFSR at random_lfsr with a polynomial of maximum
; length, generate a random word, writing it to AX.

random:

	mov ax,[ss:random_lfsr]
	shr ax,1
	jnc .sans_toggle
		xor ax,0xB400
	.sans_toggle:
	mov [ss:random_lfsr],ax

ret

times 510 - ($ - $$) db 0
dw 0xAA55
