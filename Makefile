run: matrix86.img
	qemu-system-i386 matrix86.img

matrix86.img: matrix86.s
	nasm -f bin -o matrix86.img matrix86.s

matrix86.vdi: matrix86.img
	qemu-img convert -O vdi matrix86.img matrix86.vdi

matrix86.360: matrix86.img
	cp matrix86.img matrix86.360
	dd if=/dev/zero of=matrix86.360 bs=512 seek=1 count=719

clean:
	rm -f matrix86.img matrix86.vdi
